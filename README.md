<!-- Hint: use reText markdown editor for WYSIWIG review -->

# makeProm

A program to process a Motorola S-record format data stream on **stdin** and output the data in
modified form to **stdout**.

Being that this program originated on a Unix system, on the output stream
the end-of-line is a single character: LF (\n) (0x0A).

The input stream processing can handle LF or CRLF for the end-of-line.

Originally written around 1993 for a project using Motorola 68020 CPUs.
It was for calculating and inserting a 16 bit checksum into the last
two addresses of code targeted for a 27020 EPROM.

It evolved over the years to be more versatile for both work and home projects.

Supports S2 and S3 input data record formats.  Not S1 (maybe someday for retro computing needs).

Modifications available to the output stream include:

- Inserting a 16 bit checksum at the end of the stream (default mode)
- Inserting a 32 bit CRC at the end of the stream
- Checksum is in big-Endian originally for Motorola CPUs
- CRC is in little-Endian format for ARM CPUs
- Output can be a windowed data set using -o and -e options
- Output defaults to S3 S-record format
- Output can be S2 format using -S2 option
- Output can be binary form using -b option
- Can generate even and odd byte files for 16 bit systems with two 8 bit EPROMs using -he -ho options
- See usage message for more options

To use the program:

```
makeProm [options] <inputFile >outputFile
```

Full usage information:

```
Usage: makeProm [-?] [-b|-d|-s2|-s3] [-fXX] [-rXX] [-he|-ho] [-k] [-l] [-n] [-oXXXXXX] [-eXXXXXX] [-t] < infile > outfile
 -?  - Show this usage message and exit
 -a  - append 2 byte checksum or 4 byte CRC to end of data (default is to over write last 2/4 bytes)
 -cXXXXXXXX  - 32 bit CRC with polynomial, 8 hex digits ** (instead of checksum).
 -crXXXXXXXX - 32 bit Reflected CRC with polynomial, 8 hex digits ** (instead of checksum).
 -n  - No checksum added to code, output data identical to input.
 -b  - binary output format. For making FLASH download blobs.
 -d  - db.c output format (for making relocatable code with MRI as).
 -s2 - Motorola S2 output format to stdout
 -s3 - Motorola S3 output format to stdout (default)
 -0  - Echo any S0 input string to stdout (default is to drop these records)
 -7  - Echo any S7, S8, or S9 input string to stdout (default is to drop these records)
 -fXX - set fill byte value, xx is 2 hex digits. (default is 00)
 -he - output only even bytes
 -ho - output only odd bytes
 -k  - set start address offset to first address found in input stream (default is to assume 000000)
 -l  - set end address to highest address from input stream (default is to assume end of 27020 or 27040 EPROM)
 -oXXXXXX - input file start address, 6 Hex digits (any address below is ignored.)
 -eXXXXXX - input value end address, 6 Hex digits  (any address above is ignored.)
 -q  - quiet mode. Minimal information output to stderr
 -rXX - set output S-record data length, 2 Hex digits (default 32 for S3, 16 for S2)
        Must come after -s2 or -s3 option
 -t  - test option parsing mode. no input S-record parsing, no output data to stdout
     - with CRC poly (-c) option, test will calculate CRC for "123456789" string
     ** For research info on what makes a good CRC polynomial:
	      http://users.ece.cmu.edu/~koopman/crc/crc32.html

     makeProm source available from: https://gitlab.com/mark-giebler/makeprom
```

Statistics and error messages are output to **stderr**.

## Build the Executable

Originally the program was used on a Sun UNIX platform, then support for MS-DOS was added.

Nowadays, its Linux all the way:

To make the executable:

```
gcc makeProm.c -o makeProm
```

For use in a Docker container with a possibly different version of libc6,
statically link it to prevent run time failure:

```
gcc makeProm.c -static -o makeProm
```

## CRC Notes

The CRC algorithm matches the 32 bit algorithm used by ST Microelectronics CRC peripheral
with a 32 bit polynomial fed with 8 bit input data.

For great info on what makes a good CRC polynomial, check out Philip Koopman'a research:
[http://users.ece.cmu.edu/~koopman/crc/index.html](http://users.ece.cmu.edu/~koopman/crc/index.html)

## Motorola S-Record Notes

I grew up on this format from the S19 days.
It's Awesome how a 1970's era data format is still very useful today!

Detailed format information on [Wikipedia.](https://en.wikipedia.org/wiki/SREC_(file_format))

Image on Wikipedia:

![Image: SRECORD](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/Motorola_SREC_Chart.png/1280px-Motorola_SREC_Chart.png)

# Todo

For completeness, support for S19 records should be added.
