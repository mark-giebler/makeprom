/*
 *	File Name: makeProm.c
 *
 *	Description:  v2.0
 *      This program reads a file of S records, decodes the S3/S2 records and
 *      stores the data in a large memory array corresponding to two
 *      512/1024 MByte EPROMs. (27020/27040).
 *      Then it computes the checksum for this data and stores the least
 *      significant two bytes of the checksum in the last two bytes of the
 *      array (big endian of course since this is Motorola S-records).
 *      Finally it writes out a new set of S3 records of the array
 *      data.
 *
 *      The program reads data from stdin (standard input) and writes the
 *      modified data to stdout (standard output), so the command line looks
 *      like the following:
 *                  makeProm <input.file >output.file
 *
 * 		This program originated on a Unix system, thus newline is LF (\n) (0x0A)
 *
 * 		CRC information:
 * 			Initial value used: 0xFFFFFFFF
 * 			No output XOR
 * 			Supports normal mode and Reflected mode (a.k.a. reversed)
 * 		To test (-t) that a built in input string of "123456789" generates a CRC that matches a CheckValue
 * 		Use:	-t -c04C11DB7
 * 		the resulting CRC must match CheckValue:
 *
 *		Name			Polynomial		Reflected?	Init-value		XOR-out			CheckValue
 *		crc-32-mpeg		0x04C11DB7		False		0xFFFFFFFF		0x00000000		0x0376E6E7
 *		jamcrc			0x04C11DB7		True		0xFFFFFFFF		0x00000000		0x340BC6D9
 *
 *		For Reflected test: use: -t -cr04C11DB7
 *
 *		CheckValues taken from http://crcmod.sourceforge.net/crcmod.predefined.html
 *
 * 		Known issues:
 * 			None.
 *
 * How to Build:
 * On Linux:
 * The program is built using the following command:
 *
 *		gcc makeProm.c -o makeProm
 *
 * The program is used as follows:
 *
 *		makeProm [options] <inputFile >outputFile
 *
 * Example:
 * 	./makeProm -c32c00699 -k -l <input.srec > output.srec
 * 	CRC: reflect: 0  size: 1fc24  last crc Address: 0001fc23 crc: acad2cbe
 *
 * 	Author: Mark Giebler
 *
 * ---------------------
 *	Revisions:
 * ---------------------
 * 8-Nov-2022 Mark Giebler v2.02
 * 		Fix issue of wrong EOL output on S0, S7, S8 & S9 echo when they originate
 * 		from a source that assumes Windows EOL.
 *
 * 19-Mar-2020 Mark Giebler v2.01
 * 		Re-implemented binary (-b) output mode with support for CRC injection.
 * 		Example syntax to xlate S-rec to binary as-is (no CRC or checksum injection):
 * 			./makeProm -k -l -b -n < v2.16.1006_MasterModem.srec > test.bin
 *
 * 15-Aug-2019 Mark Giebler v2.0
 * 		Released as open source.  MIT license.
 * 		Added option -c[r]XXXXXXXX - generate 32 bit CRC instead of checksum.  XXXXXXXX is polynomial
 * 			crc is inserted using little endian encoding (for ARM processor use)
 * 			-cr to use "reflected" CRC algorithm.
 * 			Various online resources used to create the CRC algorithm. Such as:
 *	 			https://en.wikipedia.org/wiki/Cyclic_redundancy_check
 * 				https://www.lammertbies.nl/forum/viewtopic.php?t=465
 * 				https://sourceforge.net/projects/crccalculator/files/CRC/
 * 				http://users.ece.cmu.edu/~koopman/crc/crc32.html
 * 		Enhanced -t mode to check a CRC output using the de-facto standard string "123456789" as input.
 * 			e.g. -t -c04C11DB7   will gen CRC: 0x0376E6E7
 *
 * 		Removed SYSTEM_GOLD build option - no longer need to build on non-ansi, really old (crica 1994) Sun unix platform.
 * 		Modified S-Record parser to echo S0 and S7/8/9 strings: Feature enabled with these options:
 * 			-0  - echo any S0... input record to stdout
 * 			-7  - echo any S7, S8, or S9 input record "as-is" to stdout.
 *		Added option -q  - quiet mode - minimal output to stderr
 *
 * 14-Aug-2019 Mark Giebler v1.9
 * 		Added support of '-' prefix on option switches to be more normative
 * 		Added option -? - show usage message and exit
 * 		Added option -a - append checksum to end of data (default over write last 2 bytes)
 * 		Added option -l to not output entire 1 MB space, but end at highest input address
 * 		Added option -k to set start address offset automatically to first address input.
 * 		Added option -fxx - set fill byte value, xx is 2 hex digits. (default is 00)
 * 		Added option -rXX - set S-record data length in bytes, 2 hex digits (default 32 for S3, 16 for S2)
 *		Added additional options settings summary and output summary info to stderr.
 *
 * 18-Apr-2001 Mark Giebler  v1.8
 *    modified so that a file buffer can be used instead of the giant
 *    prom[] memory array.  This is so it will run under stupid ol' DOS.
 *    so that DOS tools can be used to build the keypad code on various
 *    field trip code debug/fix trips with a clunky laptop which IT
 *    wouldn't let me load a *real* OS (Linux)
 *
 * 16-Feb-01 Mark Giebler  v1.7
 *      Added start offset optional parameter.
 *      Added optional output format to make assembly language module to
 *           make a code blob that is copied from ROM to execute out of
 *           RAM. This is to support FLASH programming on the ol' keypad HW.
 *           Output option 'b' format as binary to make flash download blobs.
 *      Added optional S2-record output mode '2' so ROMIT can load these files (needed for field trip to Japan).
 *      Added optional end address setting so everything above end address gets
 *           stripped out.
 *      Added option to output just the odd bytes or just the even bytes.
 *           This helps make separate files for two EPROM systems.
 *      Added option to NOT put in checksum, this makes the output smaller (no need to fill the empty spaces)
 *
 * 11-Apr-00 Mark Giebler  v1.6
 *	    Added support for 27040 EPROMs. Automatically determines best type.
 *	    Added reporting when a 27040 EPROM should be used.
 *	    Added reporting of EPROM checksum so programmer can be checked.
 * 	    Added reporting of code checksum and address it is stored at.
 *	    Added variable file length feature to get around stupid microsoft bug
 *	     when same size file replaces an old one, micorsoft DOS (WIN98/95)
 *	     does not update it's cache when file is on a network drive. STUPID bastards!!!
 *      Added ability to compile on the frickin', way-old, non-ansi compiler on gold.
 *
 * 4-Apr-97 Mark Giebler v1.5
 *      Added support for reading S2 records so that this could be used for the
 *       HQ keypad builds.
 *      Added the README info.
 *
 * 17-July-95 Mark Giebler  v1.4
 *      changed FILL_BYTE from 0xff to 0x00 to account for MRI 0 fill
 *       assumption made by MRI linker when it generates S records. (It would leave out 00 data in the s-records !)
 *
 * 11-Aug-95 Mark Giebler v1.3
 *      Major-major Clean up to improve readability of Bob's code, and usability enhancements.
 * 		Added all the comments, function headers, etc.
 *      Put in exit(0) call at end to reset exit value to 0 so make
 *       doesn't think an error happened.
 *       Printf's to stderr set non-zero return value it seems (on M$ DOS).
 *      Added printf to stderr to indicate if S records ever
 *       exceed last EPROM address. (Should actually check last
 *       address - 2 if checksum inserted).
 *      Added some extra bells and whistles to report how much of EPROM
 *       space is used. Great for determining when you need to transition
 *	 	 from 27020 to 27040.
 *
 *   V1.2 Mark Giebler
 *      Added missing reading of S2 s-record support so this can be used on HQ Keypad.
 *
 *   V1.1 Bob  Schilling (or was it Schiller?)
 *      He never documented what the changes were...he never was too good at that documenting thing...:-)
 *
 *   V1.0 Bob  First version. S3 Support only, no error reporting, no feedback, just hope and pray...
 *		no code comments, no function headers, nothing.
 *
 *
 *
 *
 *
 *
 *
 *
README:  By Mark Giebler.

(Values in parenthesis are for 27040 EPROMs).
The makeProm reads in S-Records and printf's out a new S-Record corresponding to
the data for the memory range 0-0x7ffff (0xfffff).  This program fills all unused
locations with 0x00.  (Yes, yes, 0xff would make more sense for an EPROM and speed up the programming time
BUT, the dopey SW types who made the MRI Linker, made the assumption that EPROMs are
00 filled and they would leave out long stretches of 00 bytes in their S-records.
So if you have a bunch of variables that are initialized to 00, they might get filtered out of the
S-records.  Nice huh?!  Dopes!
makeProm also computes a checksum of the bytes in locations
0 through 0x7fffd (0xffffd) and stores the lower two bytes of the sum in locations 0x7fffe (0xffffe)
and 0x7ffff (0xfffff), the LSB in the 0x7ffff (0xfffff).
  checksum = (byte[0] + byte[1] + byte[2] ...) & 0x0ffff
In summary, this program translates one
S-Record file into another with only the data needed for two (an even and odd set)
 2 mega-bit EPROM i.e. 27020 (or two 27040 EPROMS).




 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>	/* for uint32_t types */
#include <string.h>

/* define the following for DEBUG printf's to stderr */
//#define _DEBUG

/*
 define the following STUPID_DOS macro for using this on DOS with its crappy Intel processor
 and limited memory size. Avoiding that FAR/NEAR garbage.
*/
/*
#define STUPID_DOS 1
*/
/*
 * define the following STUPID_MS macro for working around Microsoft Win 98 bug with not updating
 * file if on network share.
 */
/*
#define STUPID_MS 1
 */
/*
 * Definitions
 */
#define MAX_BYTES   280                 /* Max number of bytes in S record line */
#define FILL_BYTE   0x00                /* Default Fill byte for unused locations */
										/* for MRI linker, must be 0 due to*/
										/* strange MRI feature that filters */
										/* 0 bytes if they are the first data*/
										/* in an S record. */
#define MAX_ADDR2   ((long)0x07ffffL)       /* Address of last 27020 EPROM location */
#define MAX_ADDR4   ((long)0x0fffffL)		/* last address of 27040 EPROM */

/*
 * Arrays for data storage
 */
unsigned char buf[2 * MAX_BYTES];       /* buffer for S record */
unsigned char data[MAX_BYTES];          /* decoded (binary) S record data */
unsigned char s7buf[2 * MAX_BYTES];		/* saves any input S7/8/9 record */

#ifndef STUPID_DOS
unsigned char promMem[MAX_ADDR4 + 9];    /* PROM image memory plus extra for checksum if appended to end. */
#endif

#ifdef STUPID_DOS
char bfname[]="tmp.bin";
FILE* bf;
/* read a char from bufffer file */
unsigned char promMemR(long offset)
{
	unsigned char buff[2];
	fseek(bf,offset,0);
	fread((void*)buff,1,1,bf);
	return buff[0];
}
/* read array in */
unsigned char buffer[80];
unsigned char* promMemRa(long offset, int len)
{
	fseek(bf,offset,0);
	fread((void*)buffer,1,len,bf);
	return buffer;
}
/* write a char to buffer file */
void promMemW(long offset, unsigned  char c)
{
	unsigned char buff[2];
	buf[0] = c;
	fseek(bf,offset,0);
	fwrite((void*)buff,1,1,bf);
	return;
}
/* write an array to buffer file */
void promMemWa(long offset, unsigned char* c, int len)
{
	fseek(bf,offset,0);
	fwrite((void*)c,1,len,bf);
	return;
}
/* return 1 if open, 0 if not. */
int openBuffer()
{
	if(!(bf = fopen(bfname,"w+b")))
		return 0;
	return 1;

}
void closeBuffer()
{
	fclose(bf);
}
#endif	/* STUPID_DOS */

/*
 *         Name: get2
 *  Description:
 *      Decodes 2 ASCII characters from buf & returns corresponding data byte
 *
 *    Arguments:
 *          (1) buf  (r/w)  Pointer to data to decode
 *
 *      Returns:
 *          The integer value corresponding to 2 ASCII characters, except
 *          return -1 if either character does not correspond to a hex digit
 *
 *       Author: Bob Schilling
 */
int get2(char *buf)
{
	int num;
	int chr;

	/*
	 * Decode the 1st character
	 */
	chr = buf[0];
	if (chr >= '0' && chr <= '9')
		num = (chr - '0') << 4;
	else if (chr >= 'A' && chr <= 'F')
		num = (chr - ('A' - 10)) << 4;
	else if (chr >= 'a' && chr <= 'f')
		num = (chr - ('a' - 10)) << 4;
	else
		return (-1);

	/*
	 * Decode the 2nd character and add to first
	 */
	chr = buf[1];
	if (chr >= '0' && chr <= '9')
		num += (chr - '0');
	else if (chr >= 'A' && chr <= 'F')
		num += (chr - ('A' - 10));
	else if (chr >= 'a' && chr <= 'f')
		num += (chr - ('a' - 10));
	else
		return (-1);

	return ((num&0x00ff));
}

/*
 *         Name: put2
 *  Description:
 *      Writes 2 ASCII characters to standard output corresponding to the hex
 *      value of the data byte.
 *
 *    Arguments:
 *          (1) byte  (r)  Data byte to output
 *
 *      Returns:
 *          None
 *
 *       Author: Bob Schilling
 */
void put2(unsigned char byte)
{
	int num;

	/*
	 * Output a character for the upper nibble of the byte
	 */
	num = (byte >> 4) & 0x0f;
	if (num >= 10)
		putchar(num + ('A' - 10));
	else
		putchar(num + '0');

	/*
	 * Output a character for the lower nibble of the byte
	 */
	num = byte & 0x0f;
	if (num >= 10)
		putchar(num + ('A' - 10));
	else
		putchar(num + '0');
}

/*
 *         Name: put_S3
 *  Description:
 *      Writes an S3 record to standard output corresponding to the given data.
 *      The Skip parameter when set to 2 will output every other byte, this
 *      is used to split a file into odd and even bytes for programming
 *      separate EPROMS.  For one output file skip must be 1.
 *    Arguments:
 *          (1) addr   (r)  Starting address for S3 record
 *          (2) num    (r)  Number of data bytes for record
 *          (3) bytes  (r)  Pointer to array of data bytes for record
 *          (4) skip        number of addresses to skip between each byte
 *                          output.
 *      Returns:
 *          nothing
 *
 *       Author: Bob Schilling
 * Mark Giebler: added skip parameter.  And of course the function header and comments...
 */
void put_S3(long addr, int num, unsigned char *bytes, int skip)
{
	int byte;
	int chk_sum;
	int i;

	/*
	 * Output the S3 record type
	 */
	putchar('S');
	putchar('3');

	/*
	 * Output the record length
	 */
	byte = num + 5;
	put2(byte);
	chk_sum = byte;

	/*
	 * Output the starting address of the record
	 */
	byte = (addr >> 24) & 0xff;
	put2(byte);
	chk_sum += byte;

	byte = (addr >> 16) & 0xff;
	put2(byte);
	chk_sum += byte;

	byte = (addr >> 8) & 0xff;
	put2(byte);
	chk_sum += byte;

	byte = addr & 0xff;
	put2(byte);
	chk_sum += byte;

	/*
	 * Generate the record checksum & output it with line terminator
	 */
	for (i = 0; i < num; i++)
	{
		put2(*bytes);
		chk_sum += *bytes;
		bytes += skip;
	}

	put2(~chk_sum);
	putchar('\n');
}

/*
 *         Name: put_S2
 *  Description:
 *      Writes an S2 record to standard output corresponding to the given data.
 *      The Skip parameter when set to 2 will output every other byte, this
 *      is used to split a file into odd and even bytes for programming
 *      separate EPROMS.  For one output file skip must be 1.
 *
 *    Arguments:
 *          (1) addr   (r)  Starting address for S2 record
 *          (2) num    (r)  Number of data bytes for record
 *          (3) bytes  (r)  Pointer to array of data bytes for record
 *          (4) skip        number of addresses to skip between each byte
 *                          output.
 *      Returns:
 *          nothing
 *
 *       Author: Mark Giebler
 */
void put_S2(long addr, int num, unsigned char *bytes, int skip)
{
	int byte;
	int chk_sum;
	int i;

	/*
	 * Output the S2 record type
	 */
	putchar('S');
	putchar('2');

	/*
	 * Output the record length
	 */
	byte = num + 4;
	put2(byte);
	chk_sum = byte;

	/*
	 * Output the starting address of the record
	 */

	byte = (addr >> 16) & 0xff;
	put2(byte);
	chk_sum += byte;

	byte = (addr >> 8) & 0xff;
	put2(byte);
	chk_sum += byte;

	byte = addr & 0xff;
	put2(byte);
	chk_sum += byte;

	/*
	 * Generate the record checksum & output it with line terminator
	 */
	for (i = 0; i < num; i++)
	{
		put2(*bytes);
		chk_sum += *bytes;
		bytes += skip;
	}

	put2(~chk_sum);
	putchar('\n');
}

/*
 *         Name: put_DCB
 *  Description:
 *      Writes a DC.B assembly directive with comma separated list of hex values
 * 		to standard output corresponding to the given data
 *
 *    Arguments:
 *          (1) addr   (r)  Starting address for DC.B record
 *          (2) num    (r)  Number of data bytes in bytes[] record
 *          (3) bytes  (r)  Pointer to array of bytes for output record.
 *
 *      Returns:
 *          nothing.
 *
 *       Author: Mark Giebler
 */
void put_DCB(long addr, int num, unsigned char *bytes)
{
	int i;

	/*
	 * Output DC.B directive for MRI assembler
	 */
	printf("\tDC.B\t");
	/*
	 * Generate the hex record output it with line terminator
	 */
	for (i = 0; i < num; i++)
	{
		if( i!= 0)
			putchar(',');
		putchar('$');	/* MRI prefix $ for hex values */
		put2(*bytes++);
	}
	putchar('\n');
}

// ---------------------------- CRC ------------------------------------
int crcReflectMode = 0;
unsigned int Reflect(uint32_t iReflect, const char bitDepth)
{
	unsigned int iValue = 0;
	// Swap bit 0 for bit 31, bit 1 For bit 30, etc....
	for (int iPos = 1; iPos < (bitDepth + 1); iPos++)
	{
		if (iReflect & 1)
		{
			iValue |= (1 << (bitDepth - iPos));
		}
		iReflect >>= 1;
	}
	return iValue;
}
#define WIDTH_BITS    (8 * sizeof(uint32_t))
#define TOP_BIT   (((uint32_t)1) << (WIDTH_BITS - 1))
/*
 *
 *     Function:    crcTableValue()
 *
 *  Description: Obtains the value at position for populating crc table
 *
 *       Author: Mark Giebler
 */
static uint32_t crcTableValue(uint8_t pos, uint32_t ploynomial)
{
	uint32_t crcValue = 0;
	uint8_t VP_Pos_bit = 0;

	if(crcReflectMode)
		crcValue = Reflect((uint32_t) (pos),8) << (WIDTH_BITS - 8);
	else
		crcValue = ((uint32_t) (pos)) << (WIDTH_BITS - 8);
	/* process the 8 bits */
	for (VP_Pos_bit = 0; VP_Pos_bit < 8; VP_Pos_bit++)
	{
		if (crcValue & TOP_BIT)
		{
			crcValue = (crcValue << 1) ^ ploynomial;
		}
		else
		{
			crcValue = (crcValue << 1);
		}
	}
	if(crcReflectMode)
		crcValue = Reflect(crcValue, 32);
	return (crcValue);
}

/*
 *         Name: crc32
 *  Description:
 *      Calculate the 32 bit CRC of input data using given polynomial
 *		Uses initial data of 0xffffffff
 * 		Matches 32 bit algorithm used by ST Microelectronics CRC peripheral
 * 		with 32 bit polynomial fed with 8 bit data.
 *
 *    Arguments:
 *		promData[] - array of data bytes to CRC
 * 		size - size of the data
 * 		crcPoly - 32 bit polynomial (non-reflected) to use.
 *
 *
 *      Returns:
 *          32 bit CRC of data.
 *
 *       Author: Mark Giebler
 */
unsigned int crc32(unsigned char *promData, int size, uint32_t crcPoly) {
#if(1)
	register int i;
	uint32_t crcTable[256];

	/* create lookup table */
	for (i = 0; i < 256; i++)
	{
		crcTable[i] = crcTableValue((uint8_t)(i & 0x0FF), crcPoly);
	}
	/* perform CRC calculation on promData[] array */
	register uint32_t crcCalc = 0xFFFFFFFF;
	for (i = 0; i < size; i++)
	{
		if(crcReflectMode)
			crcCalc = (crcCalc >> 8) ^ crcTable[((uint8_t)((crcCalc ) & 0x0FF)) ^ promData[i]];
		else
			crcCalc = (crcCalc << 8) ^ crcTable[((uint8_t)((crcCalc >> (WIDTH_BITS - 8)) & 0x0FF)) ^ promData[i]];
#ifdef _DEBUG
		if( i >= (size - 5))
			fprintf(stderr,"\n    CRC: reflect: %d  size: %x  last crc Address: %8.8x crc: %8.8x",crcReflectMode, size,  i, crcCalc );
#endif
	}
	return (crcCalc );
#endif
}

/*
 *         Name: usage
 *  Description:
 *      Write the usage message
 * 		to standard error.
 *
 *    Arguments:
 *			Program name.
 *
 *      Returns:
 *          nothing.
 *
 *       Author: Mark Giebler
 */
void usage( char* pname )
{
		/* usage hints */
	fprintf(stderr,"\nUsage: %s [-?] [-b|-d|-s2|-s3] [-fXX] [-rXX] [-he|-ho] [-k] [-l] [-n] [-oXXXXXX] [-eXXXXXX] [-t] < infile > outfile\n",pname);
	fprintf(stderr," -?  - Show this usage message and exit\n");
	fprintf(stderr," -a  - append 2 byte checksum or 4 byte CRC to end of data (default over write last 2/4 bytes)\n");
	fprintf(stderr," -cXXXXXXXX  - 32 bit CRC with polynomial, 8 hex digits ** (instead of checksum).\n");
	fprintf(stderr," -crXXXXXXXX - 32 bit Reflected CRC with polynomial, 8 hex digits ** (instead of checksum).\n");
	fprintf(stderr," -n  - No checksum added to code, output data identical to input.\n");
	fprintf(stderr," -b  - binary output format. For making FLASH download blobs.\n");
	fprintf(stderr," -d  - db.c output format (for making relocatable code with MRI as).\n");
	fprintf(stderr," -S2 - Motorola S2 output format to stdout\n");
	fprintf(stderr," -S3 - Motorola S3 output format to stdout(default)\n");
	fprintf(stderr," -0  - Echo any S0 input string to stdout\n");
	fprintf(stderr," -7  - Echo any S7, S8, or S9 input string to stdout\n");
	fprintf(stderr," -fXX - set fill byte value, xx is 2 hex digits. (default is %2.2X)\n", FILL_BYTE);
	fprintf(stderr," -he - output only even bytes\n");
	fprintf(stderr," -ho - output only odd bytes\n");
	fprintf(stderr," -k  - set start address offset to first address found in input stream\n");
	fprintf(stderr," -l  - set end address to highest address from input stream\n");

	fprintf(stderr," -oXXXXXX - input file start address, 6 Hex digits (any address below is ignored.)\n");
	fprintf(stderr," -eXXXXXX - input value end address, 6 Hex digits  (any address above is ignored.)\n");

	fprintf(stderr," -q  - quiet mode. Minimal information output to stderr\n");
	fprintf(stderr," -rXX - set output S-record data length, 2 Hex digits (default 32 for S3, 16 for S2)\n        Must come after -s2 or -s3 option\n");
	fprintf(stderr," -t  - test option parsing mode. no input S-record parsing, no output data to stdout\n");
	fprintf(stderr,"     - with CRC poly (-c) option, test will calculate CRC for \"123456789\" string\n");

	fprintf(stderr,"     ** For research info on what makes a good CRC polynomial:\n	      http://users.ece.cmu.edu/~koopman/crc/crc32.html\n\n");
	fprintf(stderr,"     makeProm source available from: https://gitlab.com/mark-giebler/makeprom\n");
}

/*
 *         Name: main
 *  Description:
 *      Main program to process an S record data and output the data in
 *      modified form.
 *		Calculates a 16 bit checksum or 32 bit CRC and inserts the value at the end
 * 		of the output data.  Either overwrites last 2/4 bytes or appends 2/4 bytes to end
 * 		depending on options.
 * 		See README section above for more info.
 *
 *
 *    Arguments:
 *          optional
 * 			?  - show usage message and exit
 * 			a  - append checksum  or CRC to end of data (default is to over write last 2/4 bytes)
 * 			c[r]xxxxxxxx  - Calculate a 32 bit CRC instead of checksum, xxxxxxxx is polynomial to use.
 * 			b  - output binary data
 *          d  - output in DC.B format for making an MRI assembly module.
 * 				in gcc as you can surround output with .mri 1 and .mri 0
 * 			s2 - output S2 records
 * 			s3 - output S3 records
 * 			fxx - set fill byte value, xx is 2 hex digits. (default is 00)
 * 			he - output even bytes only.
 * 			ho - output odd bytes only.
 * 			k  - set start address offset to first address found in input stream.
 * 			l  - set end address to last address from input stream.
 * 			n  - no checksum added to end of file mode.
 * 			oxxxxxx - start address offset in hex MUST be 6 digits. ignore data before address.
 * 			exxxxxx - end address. ignore data after end address.
 *			rXX - set S-record data length in bytes, 2 hex digits (default 32 for S3, 16 for S2)
 * 			0	- echo any S0 record to output
 * 			7	- echo any S7,S8,S9 end record to output as-is (default is to generate end record with start address of 0)
 * 			t  - test input options mode
 * 			q  - quiet mode - minimal information messages to stderr.
 *
 *
 *      Returns:
 *          0 if success, non-zero if error.
 *
 *       Author: Bob Schilling v1.1
 * Mark Giebler: all other versions after v1.1
 *
 * Revisions:
 *   (For complete list, see the header at the top of this file...)
 *
 *    Mark Giebler 11-apr-2000 v1.6
 *    Added support for automatically determining if EPROM will be 27020 or 27040.
 *    Added reporting of EPROM checksum so the EPROM programmer can be checked.
 *    Added reporting of the code checksum and the address it is stored at.
 *    Added feedback as lines are converted.
 *
 *    Mark Giebler 7-apr-97  v1.5
 *    Added support for S2 records so that this can be used for HQ keypad builds.
 *
 *    Mark Giebler 11-Aug-95  v1.4
 *    Added exit(0) call at end of main.  The printf to stderr sets a non-zero
 *    return code so we force it back to zero so that the makefile doesn't
 *    think this program bombed.
 *
 */
long MAX_ADDR;
unsigned char fillByte = FILL_BYTE;

int main(int argc, char *argv[])
{
	long i;
	int stopEarly = 0;	/* 1 if no checksum mode */
	int num, a, skip = 1;
	int sum = 0, test = 0;
	int lines = 0;
	int outLength = 0x20;
	long addr = -2000, offset = 0, endaddr = 0, skipaddr = 0;
	long maxAddr = 0;
	int mode = 0;			/* S3 records output mode */
	int setStartAddr = 0;	/* k - get start address from first S record line */
	int setEndAddr = 0;		/* l - get end address from last S record line */
	int append = 0; 		/* a - append checksum/CRC bytes at end instead of over writing */
	unsigned long crcPoly = 0;	/* 32 bit CRC polynomial */
	unsigned long crc = 0;		/* 32 bit CRC of code */
	int quietMode = 0;
	int echoS0 = 0;			/* echo input S0 record to stdout */
	int echoS789 = 0;		/* echo input S7/8/9 record to stdout */
	s7buf[0] = 0;			/* null the S7/8/9 save buffer */

	MAX_ADDR = MAX_ADDR2; /* default to 27020 type EPROM first */

	fprintf(stderr,"\nmakeProm version 2.02 by Mark Giebler   source available from: https://gitlab.com/mark-giebler/makeprom\n");

	/*
	 * v1.7 added options.
	 */
	a = 1;
	if( argc > 1 )
	{
		while( argc > a )
		{ /* options */
			int idx = 0;
			if( argv[a][0] == '-' ) idx=1; /* v1.9 */

			/* v2.0 switch case refactor */
			switch( argv[a][idx] )
			{
				case 't':
					test = 1;	/* test mode, no S-rec output, no S-rec reading */
					break;
				case 'd':
					mode = 1;	/* DC.B output mode */
					break;
				case 'b':
					mode = 2;	/* binary output mode */
					break;
				case 'n':
					stopEarly = 1;  /* no checksum output mode */
					break;
				case 's':
				case 'S':
					if( argv[a][idx+1] == '2' )
					{
						mode = 3;	/* S2 records output mode */
						outLength = 16;
					}
					if( argv[a][idx+1] == '3' )
						mode = 0;	/* S3 records output mode */
					break;
				case 'h':
					skip = 2;		/* split file in half */
					skipaddr = 0;	/* even bytes output (default) */
					if( argv[a][idx+1] == 'o' )
						skipaddr = 1; /* odd bytes output */
					break;
				case 'o':			/* start address offset */
					offset = 0;
					for(i = idx+1; i< idx+7; i+=2)
					{
						offset <<= 8;
						num = get2(&argv[a][i]);
						offset += num;
					}
					break;
				case 'e':			/* end address */
					endaddr = 0;
					for(i = idx+1; i< idx+7; i+=2)
					{
						endaddr <<= 8;
						num = get2(&argv[a][i]);
						endaddr += num;
					}
					MAX_ADDR = endaddr-offset; /* set new maximum */
					break;
				/* v1.9 added more options below */
				case 'f':			/* fill byte */
					fillByte = FILL_BYTE;
					num = get2(&argv[a][idx+1]);
					fillByte = num;
					break;
				case 'r':			/* output S-record data bytes per line */
					num = get2(&argv[a][idx+1]);
					outLength = num;
					if(outLength==0)
					{
						fprintf(stderr,"Error: width can't be 0\n");
						exit(-1);
					}
					break;
				case 'k':
					setStartAddr = 1;/* get start address from first S3 record */
					break;
				case 'l':
					setEndAddr = 1;	/* get end address from last S3 record address*/
					break;
				case 'a':
					append = 1;		/* append checksum to end of data instead of overwriting last bytes */
					break;
				case '?':
					usage(argv[0]);
					exit(0);
					break;
				/* v2.0 options added  */
				case 'c':			/* CRC insertion */
					if( argv[a][idx+1] == 'r' )
					{
						idx++;	// reverse mode
						crcReflectMode = 1;
					}
					crcPoly = 0;
					for(i = idx+1; i < idx+9; i+=2)
					{
						crcPoly <<= 8;
						num = get2(&argv[a][i]);
						crcPoly += num;
					}
					break;
				case '0':			/* Echo any S0 record to stdout */
					echoS0 = 1;
					break;
				case '7':
					echoS789 = 1;	/* Echo any S7/8/9 record to stdout */
					break;
				case 'q':			/* quiet mode - no output to stderr */
					quietMode = 1;
					break;
				default:
					fprintf(stderr,"Unknown option: %c\nTry -? for help\n",argv[a][idx]);
					exit(-1);
					break;
			}
			/* --- end of option parser --- */
			a++; /* next option */

		}/* end of while() */
	}else
	{
		/* no options specified */
		/* usage hints */
		usage(argv[0]);
	}
#ifndef STUPID_DOS
	/*
	 * Fill the entire PROM image array with FILL_BYTE
	 */
	for (i = 0; i <= MAX_ADDR4; i++)
		promMem[i] = fillByte;
#endif
#ifdef STUPID_DOS
	if(!(openBuffer()))
	{
		fprintf(stderr,"\nbuffer file open fail.\n");
		exit(-1);
	}
#endif

	if( !quietMode )
	{
		/* summarize the settings to be used */
		if( test )
			fprintf(stderr,"Test mode!\n");
		if( echoS0 )
			fprintf(stderr,"Echo S0 record - ON\n");
		if( echoS789 )
			fprintf(stderr,"Echo S7/S8/S9 records - ON\n");
		fprintf(stderr,"Fill (-fXX) byte: 0x%2.2X\n", (int)fillByte);
		fprintf(stderr,"Input offset (-oXXXXXX): %6.6lX  End addr: (-eXXXXXX): %6.6lX\n", offset,  endaddr);
		if( mode == 0)
			fprintf(stderr,"Output (-s3): S3-Records data width (-rXX): 0x%2.2x.\n", outLength);
		else if(mode == 1 )
			fprintf(stderr,"Output (-d): DC.B format.\n");
		else if(mode == 2)
			fprintf(stderr,"Output (-b): binary format.\n");
		else if(mode == 3) /* mode == 3  is S2 mode. */
			fprintf(stderr,"Output (-s2): S2-Records data width (-rXX): 0x%2.2x.\n", outLength);
		else
			fprintf(stderr,"Output mode: UNKNOWN!\n");

		if( crcPoly == 0)
		{	/* standard checksum */
			if( stopEarly == 0 )
				fprintf(stderr,"Checksum will %s of input data\n", append ? "appended (-a) 2 bytes to end":"overwrite last 2 bytes");
			else
				fprintf(stderr,"NO Checksum (-n) will be inserted into code.\n");
		}else
		{
			fprintf(stderr,"32-bit CRC (-c%s) will %s of input data. Polynomial: 0x%8.8lX\n",
				crcReflectMode ? "r":"",
				append ? "be appended (-a) 4 bytes to end":"overwrite last 4 bytes", crcPoly);
		}
		if(!test)
			fprintf(stderr,"Waiting for S-Record on stdin ... \n\n");
		else
			fprintf(stderr,"Test mode - ignoring input ...\n\n");
	} /* if ( !quietMode ) */
	/*
	 * Read every line of input data and process the S3/S2 records
	 */
	lines = 0;
	while( !test && fgets((char*)buf, sizeof(buf)-1 , stdin) != NULL)
	{
		buf[sizeof(buf)-1] = 0; /* make sure terminated */
		lines++;
		if ((buf[0]|0x20) == 's' && (buf[1] == '3' || buf[1] == '2'))
		{
			/*
			 * Translate every 2 HEX characters in buf into 1 byte of data
			 */
			for (i = 1; i < MAX_BYTES; i++)
			{
				num = get2((char*)(buf + i + i));
				if (num < 0)
					break;
				data[i] = num;
			}

			/*
			 * Check for valid record length
			 */
			if ((data[1] + 2) != i)
			{
				fprintf(stderr, "\nBad length on line %d!  %d, %ld",
						lines, (int)data[1], i);
				return -1;
			}

			/*
			 * Check valid checksum in the record
			 */
			sum = 0;
			num = i - 1;
			for (i = 1; i <= num; i++)
				sum += data[i];

			if ((sum & 0xff) != 0xff)
			{
				fprintf(stderr, "\nBad checksum on line %d!  0x%x, 0x%x",
						lines, ~(sum - data[num]) & 0xff, (int)data[num]);
				return -1;
			}

			/*
			 * Decode the record address and store the record data in the
			 * PROM image array
			 */
			addr = -2000;
			if( buf[1] == '3' )
				addr = (((unsigned long)data[2]) << 24) + (((unsigned long)data[3]) << 16) + (((unsigned long)data[4]) << 8) + (unsigned long)data[5];
			else
				addr = (((unsigned long)data[2]) << 16) + (((unsigned long)data[3]) << 8) + ((unsigned long)data[4]);

			if( addr >= 0l )
			{	/* have valid address */
				if( setStartAddr == 1 ) /* v1.9 */
				{	/* get offset from first line with an address */
					offset = addr;
					setStartAddr++;
					fprintf(stderr,"\rline: %6.6d addr: 0x%8.8lX   Set offset (-k): 0x%lX\n", lines, addr, offset);
				}
				addr -= offset;  /* v1.7 shift address down by offset */

				if( addr > MAX_ADDR2 && addr <= MAX_ADDR4)
					MAX_ADDR = MAX_ADDR4; /* switch to a 27040 part */
			}
			if (addr >= 0l && addr <= MAX_ADDR)
			{
				if( buf[1] == '3' )
				{
					i = 6;
				}
				else
				{
					i = 5;  /* S2 records have shorter addresses */
				}
				/* bob's obfuscated use of variables; addr is no longer the
				 true address, it is now used as an offset into data[] to where the
				 real data starts.
				 */
				addr -= i;
				/* don't exceed end of PROM image */
				/* v1.7 fixed Bob's old bug, had to add 1 otherwise we drop last byte. */
				if (num > ((MAX_ADDR+1) - addr))
					num = (MAX_ADDR+1) - addr;
#ifndef STUPID_DOS
				for (; i < num; i++)
					promMem[addr + i] = data[i];
#endif
#ifdef STUPID_DOS
				promMemWa(addr+i,&data[i],num-i);
#endif
				if((addr+i) > maxAddr)
				{
					maxAddr = addr+i;
				}
				if( !quietMode && (lines & 0x1f) == 0) /* overwrite reporting info line using /r */
					fprintf(stderr, "\rline: %6.6d addr: 0x%8.8lX   ", lines, addr);
			}else
			{
				if( addr > 0)
				fprintf(stderr, "\rWARNING: Input address 0x%8.8lX exceeded EPROM size 0x%lx on line %d, skipping it..\n",
					addr+offset, MAX_ADDR+1ul, lines);
				else
				fprintf(stderr, "\rWARNING: Input address 0x%8.8lX less than start offset on line %d, skipping it..\n",
					addr+offset, lines);
			}
		}else if( echoS0 == 1 && (buf[0]|0x20) == 's' && buf[1] == '0' )
		{
			// remove all EOL in case dealing with Windows EOL on input stream.
			buf[strcspn(buf, "\r\n")] = 0; // works for LF, CR, CRLF, LFCR, ...
			printf("%s\n",(char*)buf);
			echoS0 = 2;
		}else  if( echoS789 == 1  && (buf[0]|0x20) == 's' && (buf[1] == '7' || buf[1] == '8' || buf[1] == '9' ))
		{
			// remove all EOL in case dealing with Windows EOL on input stream.
			buf[strcspn(buf, "\r\n")] = 0; // works for LF, CR, CRLF, LFCR, ...
			/* need to save record for output as last record */
			strncpy(s7buf, buf, 48);
			s7buf[48]=0;
			echoS789 = 2; 	/* indicate we have an S7/8/9 record so we dont generate another */
		}
		else
		{
			fprintf(stderr,"\rline: %6.6d addr: 0x%8.8lX   Ignoring Line: [%c%c]\n", lines, addr>=0?addr:0, buf[0], buf[1]);
		}
	}	/* end of while() */

	/* -------------------------------------------------------------- */
	/* ------------------ now output in requested format ------------ */
	/* -------------------------------------------------------------- */

	/* report last line and max address- overwrite any previous report */
	fprintf(stderr, "\rline: %6.6d max addr: 0x%8.8lX   \n", lines, maxAddr);

	if( !test && maxAddr > 0 )
	{
		/* v1.9 */
		if( setEndAddr == 1 )
		{
			/* change MAX_ADDR address to match last addr in input stream */
			MAX_ADDR = maxAddr-1;
		}
		if( setStartAddr == 0 )
		{
			offset = 0;	/* output address starts at 0000 */
		}
		if( stopEarly )
			endaddr = maxAddr; /* no checksum mode */
		else
			endaddr = MAX_ADDR;/* end address for checksum calculation */

		if( !quietMode && (setStartAddr || setEndAddr ) )
			fprintf(stderr,"\nStart offset (-k): 0x%8.8lX  End addr (-l): 0x%8.8lX", offset,  endaddr);

		if( append && stopEarly == 0 )
		{	/* adjust MAX_ADDR such that checksum is appended after last input address */
			if( crcPoly == 0 )
				MAX_ADDR += 2; /* Checksum take 2 bytes */
			else
				MAX_ADDR += 4; /* CRC takes 4 bytes */
		}

		if(mode == 0)
		{

			/* --------------   s3 record output mode  -------------- */
			/*
			 * Calculate the checksum of the PROM image array
			 * We force last two locations to 00 00 for now.
			 */
			sum = 0;
			if( stopEarly == 0 )
			{
				/* last two bytes are replaced with checksum */
				promMem[MAX_ADDR - 1] = 0;
				promMem[MAX_ADDR] = 0;
				if( crcPoly != 0 )
				{
					/* last 4 bytes if CRC mode */
					promMem[MAX_ADDR - 2] = 0;
					promMem[MAX_ADDR - 3] = 0;
				}
			}
#ifndef STUPID_DOS
			if( crcPoly == 0 )
			{	/* calculate checksum */
				for (addr = skipaddr; addr < (MAX_ADDR + 1); addr+=skip)
					sum += promMem[addr];
			}else
			{
				/* calculate CRC. Don't include last 32 bits if in overwrite mode */
				crc = crc32(&promMem[0], append?(endaddr+1):(endaddr+1-4), crcPoly);
			}
#endif
#ifdef STUPID_DOS
			if( crcPoly == 0 )
			{	/* calculate checksum */
				for (addr = skipaddr; addr < (MAX_ADDR + 1); addr+=skip)
					sum += promMemR(addr);
			}else
			{
				/* calculate CRC */
			}
#endif
			if( skip == 1 && stopEarly == 0 && crcPoly == 0) /* normal mode.. */
			{
				/*
				 * store check sum in last two eprom locations. Big Endian format.
				 */
#ifndef STUPID_DOS
				promMem[MAX_ADDR - 1] = (sum >> 8) & 0x0ff;
				promMem[MAX_ADDR] = sum & 0x0ff;
				/*
				 *  add in the code checksum to obtain the EPROM checksum
				 */
				sum += promMem[MAX_ADDR -1] + promMem[MAX_ADDR];
#endif
#ifdef STUPID_DOS
				promMemW((MAX_ADDR-1),((sum >> 8 ) & 0x0ff));
				promMemW((MAX_ADDR), (sum & 0x0ff));
				sum += promMemR(MAX_ADDR-1) + promMemR(MAX_ADDR);
#endif
			}else if( skip == 1 && stopEarly == 0 && crcPoly != 0 )
			{
				/*
				 * store 32 bit CRC in last four eprom locations in Little Endian format.
				 */
				promMem[MAX_ADDR - 3] = (crc) & 0xff;
				promMem[MAX_ADDR - 2] = (crc >> 8) & 0xff;
				promMem[MAX_ADDR - 1] = (crc >> 16) & 0xff;
				promMem[MAX_ADDR]     = (crc >> 24) & 0x0ff;
			}

			/*
			 * Output S3 records for the entire PROM image array
			 */
			if( stopEarly )
				endaddr = maxAddr; /* no checksum mode */
			else
				endaddr = MAX_ADDR;
#ifndef STUPID_DOS
			for (addr = 0; skipaddr <= endaddr; addr += outLength,skipaddr += (outLength*skip))
				put_S3(addr+offset, (endaddr-skipaddr)>=outLength?outLength:(endaddr-skipaddr+1), &promMem[skipaddr], skip);
#endif
#ifdef STUPID_DOS
			for (addr = 0; skipaddr <= endaddr; addr += outLength,skipaddr += (outLength*skip))
				put_S3(addr+offset, outLength, promMemRa(skipaddr,outLength), skip);
#endif
			/*
			 * Output a terminator record
			 */
			if( echoS789 == 2 )
			{
				printf("%s\n",s7buf);	/* echo input end record as-is */
				echoS789 = 3;
			}
			else
				printf("S70500000000FA\n");
		}
		else if( mode == 1) /* DC.B mode */
		{
			printf("* automatically generated file. DO NOT MODIFY!\n\n");
			printf("\tsection ram_blob\n\n");
			printf("\tXDEF\t_blob_start\n");
			printf("\tXDEF\t_blob_end\n\n");
			printf("_blob_start:\n");
			/*
			 * Output DCB array for image loaded.
			 */
#ifndef STUPID_DOS
			for (addr = 0; addr <= maxAddr+2; addr += 8)
				put_DCB(addr, 8, &promMem[addr]);
#endif
#ifdef STUPID_DOS
			for (addr = 0; addr <= maxAddr+2; addr += 8)
				put_DCB(addr, 8, promMemRa(addr,8));
#endif
			printf("\n\n_blob_end:\n");
		}
		else if(mode == 2 )
		{
			/* ------------ binary output mode ---------------------- */
			/*
			 * Calculate the checksum of the PROM image array
			 * We force last two locations to 00 00 for now.
			 */
			sum = 0;
			if( stopEarly == 0 )
			{
				/* last two bytes are replaced with checksum */
				promMem[MAX_ADDR - 1] = 0;
				promMem[MAX_ADDR] = 0;
				if( crcPoly != 0 )
				{
					/* last 4 bytes if CRC mode */
					promMem[MAX_ADDR - 2] = 0;
					promMem[MAX_ADDR - 3] = 0;
				}
			}
#ifndef STUPID_DOS
			if( crcPoly == 0 )
			{	/* calculate checksum */
				for (addr = skipaddr; addr < (MAX_ADDR + 1); addr+=skip)
					sum += promMem[addr];
			}else
			{
				/* calculate CRC. Don't include last 32 bits if in overwrite mode */
				crc = crc32(&promMem[0], append?(endaddr+1):(endaddr+1-4), crcPoly);
			}
#endif
#ifdef STUPID_DOS
			if( crcPoly == 0 )
			{	/* calculate checksum */
				for (addr = skipaddr; addr < (MAX_ADDR + 1); addr+=skip)
					sum += promMemR(addr);
			}else
			{
				/* calculate CRC */
				crc = crc32(&promMem[0], append?(endaddr+1):(endaddr+1-4), crcPoly);
			}
#endif
			/* update the memory image with the checksum or CRC result */
			if( skip == 1 && stopEarly == 0 && crcPoly == 0) /* normal mode.. */
			{
				/*
				 * store check sum in last two eprom locations. Big Endian format.
				 */
#ifndef STUPID_DOS
				promMem[MAX_ADDR - 1] = (sum >> 8) & 0x0ff;
				promMem[MAX_ADDR] = sum & 0x0ff;
				/*
				 *  add in the code checksum to obtain the EPROM checksum
				 */
				sum += promMem[MAX_ADDR -1] + promMem[MAX_ADDR];
#endif
#ifdef STUPID_DOS
				promMemW((MAX_ADDR-1),((sum >> 8 ) & 0x0ff));
				promMemW((MAX_ADDR), (sum & 0x0ff));
				sum += promMemR(MAX_ADDR-1) + promMemR(MAX_ADDR);
#endif
			}else if( skip == 1 && stopEarly == 0 && crcPoly != 0 )
			{
				/*
				 * store 32 bit CRC in last four eprom locations in Little Endian format.
				 */
				promMem[MAX_ADDR - 3] = (crc) & 0xff;
				promMem[MAX_ADDR - 2] = (crc >> 8) & 0xff;
				promMem[MAX_ADDR - 1] = (crc >> 16) & 0xff;
				promMem[MAX_ADDR]     = (crc >> 24) & 0x0ff;
			}
			if( stopEarly )
				endaddr = maxAddr-1; /* no checksum mode */
			else
				endaddr = MAX_ADDR;
#
#ifndef STUPID_DOS
			/* output the binary to stdout */
			for (addr = skipaddr; addr <= endaddr; addr+=skip )
			{
				putchar(promMem[addr]);
			}
#endif
#ifdef STUPID_DOS
			/*
			for (addr = skipaddr; addr <= endaddr; addr+=skip )
			{
				rc = promMemR(addr);
				putc(rc,stdout);
				sum += rc;
			}
			*/
#endif
		}
		else if (mode == 3 )
		{
			/* --------------  S2 output mode ----------------------- */
			/*
			 * Calculate the checksum of the PROM image array
			 * We force last two locations to 00 00 for now.
			 */
			sum = 0;
			if( stopEarly == 0 )
			{
				/* last two bytes are replaced with checksum */
				promMem[MAX_ADDR - 1] = 0;
				promMem[MAX_ADDR] = 0;
				if( crcPoly != 0 )
				{
					/* last 4 bytes if CRC mode */
					promMem[MAX_ADDR - 2] = 0;
					promMem[MAX_ADDR - 3] = 0;
				}
			}
#ifndef STUPID_DOS
			if( crcPoly == 0 )
			{	/* calculate checksum */
				for (addr = skipaddr; addr < (MAX_ADDR + 1); addr+=skip)
					sum += promMem[addr];
			}else
			{
				/* calculate CRC */
				crc = crc32(&promMem[0], endaddr-skipaddr , crcPoly);
			}
#endif
#ifdef STUPID_DOS
			if( crcPoly == 0 )
			{	/* calculate checksum */
				for (addr = skipaddr; addr < (MAX_ADDR + 1); addr+=skip)
					sum += promMemR(addr);
			}else
			{
				/* calculate CRC */
			}
#endif
			if( skip == 1 && stopEarly == 0 && crcPoly == 0 )
			{	/* not splitting odd/even so can insert checksum */
				/*
				 * store check sum in last two eprom locations in Big Endian format.
				 */
#ifndef STUPID_DOS
				promMem[MAX_ADDR - 1] = (sum >> 8) & 0xff;
				promMem[MAX_ADDR] = sum & 0x0ff;
				/*
				 *  add in the code checksum to obtain the EPROM checksum
				 */
				sum += promMem[MAX_ADDR -1] + promMem[MAX_ADDR];
#endif
#ifdef STUPID_DOS
				promMemW((MAX_ADDR-1),((sum >> 8 ) & 0xff));
				promMemW((MAX_ADDR), (sum & 0x0ff));
				sum += promMemR(MAX_ADDR-1) + promMemR(MAX_ADDR);
#endif
			}else if( skip == 1 && crcPoly != 0 )
			{
				/*
				 * store 32 bit CRC in last four eprom locations in Little Endian format.
				 */
				promMem[MAX_ADDR - 3] = (crc) & 0xff;
				promMem[MAX_ADDR - 2] = (crc >> 8) & 0xff;
				promMem[MAX_ADDR - 1] = (crc >> 16) & 0xff;
				promMem[MAX_ADDR]     = (crc >> 24) & 0x0ff;
			}
			/*
			 * Output S2 records for the entire PROM image array
			 */
			if( stopEarly )	/* no checksum */
				endaddr = maxAddr;
			else
				endaddr = MAX_ADDR;
#ifndef STUPID_DOS
			for (addr = 0; skipaddr <= endaddr; addr += outLength,skipaddr += (outLength*skip))
				put_S2(addr, (endaddr-skipaddr)>=outLength?outLength:(endaddr-skipaddr+1), &promMem[skipaddr],skip);
#endif
#ifdef STUPID_DOS
			for (addr = 0; skipaddr <= endaddr; addr += outLength,skipaddr += (outLength*skip))
				put_S2(addr, outLength, promMemRa(skipaddr,outLength),skip);
#endif
			/*
			 * Output a terminator record
			 */
			if( echoS789 == 2 )
			{
				printf("%s\n",s7buf);	/* echo input end record as-is */
				echoS789 = 3;
			}
			else
				printf("S804000000FB\n");
		}
	}/* end of if !test */

	if(test && crcPoly != 0 )
	{
		// run a check of CRC polynomial with input "123456789".
		memcpy(promMem, "123456789", 9);
		/* calculate CRC */
		crc = crc32(&promMem[0], 9 , crcPoly);
	}

	/* ------------------ summarize operation ----------------------- */
		fprintf(stderr, "\n\ninput lines: %d   output lines: %ld ", lines, maxAddr > 0 ? (addr / outLength):0);

		if( skip == 1 && stopEarly == 0 )
		{
			if( crcPoly == 0 )
			{
				fprintf(stderr," EPROM CheckSum: %4.4X", (sum & 0x0ffffff) );
#ifndef STUPID_DOS
				sum = promMem[MAX_ADDR - 1] << 8;
				sum += promMem[MAX_ADDR];
#endif
#ifdef STUPID_DOS
				sum = promMemR(MAX_ADDR - 1) << 8;
				sum += promMemR(MAX_ADDR);
#endif
				fprintf(stderr,"\nCode Checksum: %4.4X at address: 0x%8.8lX  (Big Endian)",(sum & 0x0ffff),MAX_ADDR-1);
			}else
			{
				fprintf(stderr,"\nCode CRC: %8.8lX at address: 0x%8.8lX  (Little Endian)",(crc ), MAX_ADDR-3);
			}
		}
	if( !quietMode )
	{

		if( echoS0 == 2 )
			fprintf(stderr,"\nEchoed input S0 record");
		if( echoS789 == 3 )
			fprintf(stderr,"\nEchoed input S7/8/9 record");

		if( mode == 0 || mode == 3)
		{	/* output: S3-Records or S2 */
#ifndef STUPID_DOS
			fprintf(stderr, "\nMax code address = 0x%8.8lX (%ld%% of EPROM used)  (%ld KB)\n",maxAddr-1,((maxAddr-1)*100)/MAX_ADDR, maxAddr/1024l );
#endif
#ifdef STUPID_DOS
			fprintf(stderr, "\nMax code address = 0x%8.8lX  (%ld KB)\n",maxAddr-1, maxAddr/1024l );
#endif
			if( MAX_ADDR == MAX_ADDR4 )
				fprintf( stderr,"\n***  EPROM type is 27040 (1024 KB) *********\n\n");
			else
				fprintf( stderr,"\nEPROM type is 27020 (512 KB)\n\n");
		}else
		{
			fprintf(stderr, "\nMax code address = 0x%8.8lX  (%ld KB)\n",maxAddr-1, maxAddr/1024l );
		}
	} /* if ( !quietMode ) */
	else
	{
		fprintf(stderr,"\n");
	}
#ifdef STUPID_MS
	/*
	 * make file length vary based on last hex digit of checksum
	 *  this is to get around a stupid microsoft bug when this file is copied
	 *  over an old one with the same size and same name.  It wouldn't save!
	 *
	 */
	if( mode != 2 ) /* if not binary mode do this */
		for( i = 0; i <= (sum &0x0f); i++)
			putchar('\n');
#endif
#ifndef STUPID_DOS
/* exit with 0 so errorno gets reset to zero after all those fprintfs to stderr */
	exit(0);
#endif
#ifdef STUPID_DOS
	closeBuffer();
#endif
	return 0;  /* to keep some silly compilers from bitching about no return value, even though we exit(0)... */
}
